#ifndef LOG_H
#define LOG_H

void error(char * err);
void info(char * info);
void warn(char * wrn);
void pass(char * pss);

char assertEqualsPtr(void * a, void * b, char * name);
char assertEqualsInt(int a, int b, char * name);
char assertEqualsChar(char a, char b, char * name);
char assertEqualsString(char * a, char * b, int alen, int blen, char * name);

#endif