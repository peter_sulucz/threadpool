#include "Log.h"
#include <stdio.h>
#include <string.h>
#include "Colors.h"

#ifdef _WIN32

// ignore warnings in this file...
#pragma warning(push, 3) 
#include <Windows.h>
#pragma warning(pop)

static HANDLE hConsole = 0;

static void setup(void)
{
	hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

}

void error(char * err)
{
	if (!hConsole) setup();
	SetConsoleTextAttribute(hConsole, 4);
	fprintf(stdout, "[FAIL]");
	SetConsoleTextAttribute(hConsole, 15);
	fprintf(stdout, "%s\n", err);
}
void info(char * info)
{
	if (!hConsole) setup();
	SetConsoleTextAttribute(hConsole, 9);
	fprintf(stdout, "[INFO]");
	SetConsoleTextAttribute(hConsole, 15);
	fprintf(stdout, "%s\n", info);
}
void warn(char * wrn)
{
	if (!hConsole) setup();
	SetConsoleTextAttribute(hConsole, 6);
	fprintf(stdout, "[WARN]");
	SetConsoleTextAttribute(hConsole, 15);
	fprintf(stdout, "%s\n", wrn);
}
void pass(char * pss)
{
	if (!hConsole) setup();
	SetConsoleTextAttribute(hConsole, 2);
	fprintf(stdout, "[PASS]");
	SetConsoleTextAttribute(hConsole, 15);
	fprintf(stdout, "%s\n", pss);
}

#else
void error(char * err)
{
	fprintf(stdout, RED "[FAIL]" NORMAL " %s\n", err);
}
void info(char * info)
{
	fprintf(stdout, BLUE "[INFO]" NORMAL " %s\n", info);
}
void warn(char * wrn)
{
	fprintf(stdout, YELLOW "[WARNING]" NORMAL " %s\n", wrn);
}
void pass(char * pss)
{
	fprintf(stdout, GREEN "[PASS]" NORMAL " %s\n", pss);
}
#endif

char assertEqualsPtr(void * a, void * b, char * name)
{
	if (a == b)
		pass(name);
	else
		error(name);

	return a == b;
}

char assertEqualsInt(int a, int b, char * name)
{
	if (a == b)
		pass(name);
	else
		error(name);

	return a == b;
}
char assertEqualsChar(char a, char b, char * name)
{
	if (a == b)
		pass(name);
	else
		error(name);

	return a == b;
}

char assertEqualsString(char * a, char * b, int alen, int blen, char * name)
{
	int i = 0;
	int j = 0;
	while (i < alen && j < blen && a[i] == b[j]){ i++; j++; };
	if (i == alen && j == blen)
	{
		pass(name);
		return 1;
	}
	error(name);
	return 0;
}