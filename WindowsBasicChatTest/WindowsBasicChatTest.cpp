// WindowsBasicChatTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdlib.h>
#include "../MessageRouter/Router.h"
#include "../BasicTestFramework/Log.h"

/*
	Get an ip address from a.b.c.d
*/
static unsigned long long getip(unsigned char a, unsigned char b, unsigned char c, unsigned char d)
{
	return (unsigned long long)a << 24 | (unsigned long long)b << 16 | (unsigned long long)c << 8 | (unsigned long long)d;
}

int _tmain(int argc, _TCHAR* argv[])
{
	Router router;
	char inptbuffer[256];
	int port;
	fprintf_s(stdout, "Which port to start on?\n");
	fscanf_s(stdin, "%d", &port);
	//int port = atoi(inptbuffer);
	if (ROUTER_init(&router, port))
	{
		pass("Router Started");
	}
	else{
		error("Router Started");
	}

	fprintf(stdout, "READY (Sepearte ip with dots.)\nEX: CONN 192.168.1.1 2091\nEXIT to quit\n");
	fgets(inptbuffer, 256, stdin); // get the newline
	while (1)
	{
		memset(inptbuffer, 0, 256);
		fgets(inptbuffer, 256, stdin);
		if (strstr(inptbuffer, "CONN") == inptbuffer)
		{
			char * ip = inptbuffer + 5;
			unsigned char a, b, c, d;
			int port;
			sscanf_s(ip, "%hhu.%hhu.%hhu.%hhu %i", &a, &b, &c, &d, &port);

			unsigned long long addr = getip(a, b, c, d);
			
			ROUTER_createConnection(&router, htonl((u_long)addr), htons(port));
		}
		else if (strstr(inptbuffer, "EXIT") == inptbuffer)
		{
			break;
		}
		else
		{
			for (int i = 0; i < 256; i++)
			{
				if (inptbuffer[i] == '\n' || inptbuffer[i] == '\r')
				{
					inptbuffer[i] = 0;
					break;
				}
			}
			ROUTER_broadcastALL(&router, inptbuffer, 256);
		}
	}

	ROUTER_destroy(&router);

	return 0;
}

