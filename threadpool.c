#include "threadpool.h"

typedef struct _exec_payload {
	ThreadPool * pool;
	int index;
} ExecPayload;

static void InitializeThreads(ThreadPool * pool);
static void exec(void * payload);

char POOL_Init(ThreadPool * pool, int threads)
{
	pool->threads = (Thread *)malloc(sizeof(Thread)* threads);
	pool->numThreads = threads;
	pool->isRunning = 1;
	pool->globalSemaphore = CreateSemaphore(NULL, 0, 10000, NULL);
	pool->globalListMutex = CreateMutex(NULL, 0, NULL);
	LINKEDLIST_Init(&pool->GlobalQueue);
	InitializeThreads(pool);
	return 1;
}

void POOL_Destroy(ThreadPool * pool)
{
	pool->isRunning = 0;
	for (int i = 0; i < pool->numThreads; i++)
	{
		TerminateThread(&pool->threads[i].thread, THREAD_TERMINATE);
	}
}

/// Add an item to the pool to execute
void POOL_exec(ThreadPool * pool, DelegateWorker * del)
{
	WaitForSingleObject(pool->globalListMutex, INFINITE);
	LINKEDLIST_InitNode(&del->elem);
	LINKEDLIST_Push(&pool->GlobalQueue, &del->elem);
	ReleaseSemaphore(pool->globalSemaphore, 1, 0);
	ReleaseMutex(pool->globalListMutex);
}

static void InitializeThreads(ThreadPool * pool)
{
	for (int i = 0; i < pool->numThreads; i++)
	{
		Thread t = pool->threads[i];
		t.semaphore = CreateSemaphore(NULL, 0, 200, NULL);
		ExecPayload * payload = (ExecPayload *)malloc(sizeof(ExecPayload));
		payload->pool = pool;
		payload->index = i;
		t.thread = _beginthread(exec, 0, payload);
	}
}

static void exec(void * payload)
{
	ExecPayload * pyld = (ExecPayload *)payload;
	while (pyld->pool->isRunning)
	{
		WaitForSingleObject(pyld->pool->globalSemaphore, INFINITE);
		WaitForSingleObject(pyld->pool->globalListMutex, INFINITE);
		DelegateWorker * del = GetElement(LINKEDLIST_Pop(&pyld->pool->GlobalQueue), DelegateWorker, elem);
		ReleaseMutex(pyld->pool->globalListMutex);
		del->func(del->args);
		free(del);
	}
}