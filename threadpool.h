#ifndef THREADPOOL_H
#define THREADPOOL_H
#pragma warning(push, 3)
#include <stdlib.h>
#include <stddef.h>
#include <Windows.h>
#include <process.h>
#pragma warning(pop)
#include "List\list.h"

typedef struct _thread{
	uintptr_t thread;
	HANDLE semaphore;
} Thread;

#pragma pack(push, 1)
typedef __declspec(align(1)) struct _thread_pool {
	Thread * threads;
	int numThreads;
	LinkedList GlobalQueue;
	HANDLE globalListMutex;
	HANDLE globalSemaphore;
	volatile char isRunning;
} ThreadPool;
#pragma pack(pop)

typedef void (__cdecl * Delegate)(void *);

typedef struct _delegate_item{
	Delegate func;
	void * args;
	LinkedListNode elem;
} DelegateWorker;

char POOL_Init(ThreadPool * pool, int threads);

void POOL_Destroy(ThreadPool * pool);

void POOL_exec(ThreadPool * pool, DelegateWorker * del);

#define GetElement(ElemPtr, Type, ElemName) (Type *)((unsigned)ElemPtr - offsetof(Type, ElemName))

#endif