#include "list.h"
#include <stdlib.h>
#include <assert.h>

/*
	Pop an item from the front of the list
*/
LinkedListNode * LINKEDLIST_Pop(LinkedList * list) {
	assert(list->count > 0);
	LinkedListNode * node = list->head;
	if(LINKEDLIST_remove(list, node))
		return node;
	return NULL;
}

/*
	Push an item to the front of the list
*/
char LINKEDLIST_Push(LinkedList * list, LinkedListNode * node) {
	// if the nodes next and prev are not null, LINKEDLiST_InitNode may not have been called
	assert(node->next == NULL && node->prev == NULL);

	// if the list is completely empty, it's easier to just add
	if (list->count == 0)
	{
		return LINKEDLIST_add(list, node);
	}
	
	// list is not empty, so add to the front.
	list->head->prev = node;
	node->next = list->head;
	list->head = node;
	list->count++;

	//1 success
	return 1;
}

/*
	Initialize the linkedlist.
*/
char LINKEDLIST_Init(LinkedList * list)
{
	list->head = NULL;
	list->tail = NULL;
	list->count = 0;
	return 1;
}

/*
	Initialize a listnode. Call before inserting node
*/
char LINKEDLIST_InitNode(LinkedListNode * node)
{
	node->next = NULL;
	node->prev = NULL;
	return 1;
}

/*
	Destroy the linkedlist.
*/
char LINKEDLIST_Destroy(LinkedList * list)
{
	assert(list->count == 0); // fail if the list is empty
	list->head = NULL;
	list->tail = NULL;
	list->count = 0;
	free(list);
	return 1;
}

/*
	What is the count?
*/
unsigned int LINKEDLIST_length(LinkedList * list)
{
	return list->count;
}

/*
	Add an item to the back of the linkedlist. 
*/
char LINKEDLIST_add(LinkedList * list, LinkedListNode * node)
{
	assert(node->next == NULL && node->prev == NULL && list->head != node); // make sure we arent already in the list..

	// list is empty... set as the head node
	if (list->head == NULL)
	{
		list->head = node;
	}

	// set the tail node
	node->prev = list->tail;
	
	if (list->tail != NULL)
	{
		list->tail->next = node;
	}
	list->tail = node;

	// dont forget to increase the count
	list->count++;
	return 1;
}

/*
	Remove a specific item from the linkedlist
*/
char LINKEDLIST_remove(LinkedList * list, LinkedListNode * node)
{
	assert(node->next != NULL || node->prev != NULL || list->head == node); // not in the list...

	if (node->next != NULL)
	{
		node->next->prev = node->prev;
	}
	if (node->prev != NULL)
	{
		node->prev->next = node->next;
	}
	if (list->head == node)
		list->head = node->next;
	if (list->tail == node)
		list->tail = node->prev;

	node->prev = NULL;
	node->next = NULL;
	list->count--;

	return 1;
}