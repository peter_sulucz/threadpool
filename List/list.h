#ifndef LIST_H
#define LIST_H

typedef struct _linkedlistnode{
	struct _linkedlistnode * prev;
	struct _linkedlistnode * next;
} LinkedListNode;

typedef struct _linkedlist{
	LinkedListNode * head;
	LinkedListNode * tail;
	unsigned int count;
} LinkedList;

LinkedListNode * LINKEDLIST_Pop(LinkedList * list);
char LINKEDLIST_Push(LinkedList * list, LinkedListNode * node);
char LINKEDLIST_Init(LinkedList * list);
char LINKEDLIST_InitNode(LinkedListNode * node);
char LINKEDLIST_Destroy(LinkedList * list);
unsigned int LINKEDLIST_length(LinkedList * list);
char LINKEDLIST_add(LinkedList * list, LinkedListNode * node);
char LINKEDLIST_remove(LinkedList * list, LinkedListNode * node);

#define GetListNode(ElemPtr, Type, Elemname) (ElemPtr - offsetof(Type, Elemname))

#endif