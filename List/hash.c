#include "hash.h"
#include <stdlib.h>

char HASH_init(HashTable * hash, unsigned int len)
{
	hash->hash = (hashKeyVal **)malloc(sizeof(hashKeyVal *)* len);

	// clear the array
	for (unsigned int i = 0; i < len; i++)
		hash->hash[i] = NULL;

	hash->len = len;
	hash->count = 0;
	return 1;
}

char HASH_add(HashTable * hash, unsigned long long key, void * value)
{
	unsigned int index = key % hash->len;
	hashKeyVal * nw = (hashKeyVal *)malloc(sizeof(hashKeyVal));
	nw->key = key;
	nw->next = hash->hash[index];
	nw->value = value;
	hash->hash[index] = nw;
	return 1;
}

char HASH_remove(HashTable * hash, unsigned long long key)
{
	unsigned int index = key % hash->len;
	hashKeyVal * kv = hash->hash[index];
	if (kv->key == key)
	{
		hash->hash[index] = kv->next;
		kv->key = 0;
		kv->next = NULL;
		kv->value = NULL;
		free(kv);
		return 1;
	}
	while (kv->next != NULL)
	{
		if (kv->next->key == key)
		{
			hashKeyVal * todelete = kv->next;
			kv->next = todelete->next;
			todelete->key = 0;
			todelete->value = NULL;
			todelete->next = NULL;
			free(todelete);
			return 1;
		}
	}
	return 0;
}

void * HASH_get(HashTable * hash, unsigned long long key)
{
	unsigned int index = key % hash->len;
	hashKeyVal * kv = hash->hash[index];
	while (kv != NULL)
	{
		if (kv->key == key)
			return kv->value;
		kv = kv->next;
	}
	return NULL;
}