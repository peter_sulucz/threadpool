#ifndef _HASH_H
#define _HASH_H

typedef struct _hashkeyval{
	unsigned long long key;
	void * value;
	struct _hashkeyval * next;
} hashKeyVal;

typedef struct _hashtable{
	hashKeyVal ** hash;
	unsigned int len; 
	unsigned int count;
} HashTable;

char HASH_init(HashTable * hash, unsigned int len);

char HASH_add(HashTable * hash, unsigned long long key, void * value);

char HASH_remove(HashTable * hash, unsigned long long key);

void * HASH_get(HashTable * hash, unsigned long long key);

#endif