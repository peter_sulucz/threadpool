#include "stdafx.h"
#include "RouterTests.h"
#include "../BasicTestFramework/Log.h"
#include "../MessageRouter/Router.h"

void RunRouterTests(void)
{
	info("Starting router tests");

	Router ra;
	Router rb;

	ROUTER_init(&ra, 1024);
	ROUTER_init(&rb, 1025);
	
	Sleep(2000);

	ROUTER_createConnection(&ra, 2130706433 /* localhost */, 1025);

	Sleep(2000);

	ROUTER_destroy(&ra);
	ROUTER_destroy(&rb);

	info("Finished router tests");
	return;
}