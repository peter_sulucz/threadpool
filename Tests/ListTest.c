#include "stdafx.h"
#include "ListTest.h"
#include "../BasicTestFramework/Log.h"
#include "../List/list.h"
#include <stdlib.h>

static void TestInit(void)
{

	info("Init tests");
	LinkedList * list = (LinkedList *)malloc(sizeof(LinkedList));
	LINKEDLIST_Init(list);
	assertEqualsPtr(list->head, NULL, "List head");
	assertEqualsPtr(list->tail, NULL, "List tail");
	assertEqualsInt(list->count, 0, "List Count");
	
	free(list);
}

static void TestNode(void)
{
	info("Testing List Nodes");
	LinkedListNode node;
	LINKEDLIST_InitNode(&node);
	assertEqualsPtr(node.next, NULL, "Node Next");
	assertEqualsPtr(node.prev, NULL, "Node Previous");
}

static void TestAdd(void)
{
	info("Add tests");
	LinkedList * list = (LinkedList *)malloc(sizeof(LinkedList));
	LINKEDLIST_Init(list);
	
	LinkedListNode nodes[10];
	for (int i = 0; i < 10; i++)
	{
		LINKEDLIST_InitNode(&nodes[i]);
		LINKEDLIST_add(list, &nodes[i]);
	}

	assertEqualsPtr(list->head, &nodes[0], "Head Pointer");
	assertEqualsPtr(list->tail, &nodes[9], "Tail Pointer");
	assertEqualsInt(list->count, 10, "List Count");
	assertEqualsPtr(list->head->next, &nodes[1], "List Next Pointer");
	assertEqualsPtr(list->tail->prev, &nodes[8], "List Prev Pointer");
	assertEqualsPtr(list->head->prev, NULL, "Head prev pointer");
	assertEqualsPtr(list->tail->next, NULL, "Tail next Pointer");

	free(list);
}

static void TestRemove(void)
{
	info("Remove Tests");
	LinkedList * list = (LinkedList *)malloc(sizeof(LinkedList));
	LINKEDLIST_Init(list);

	LinkedListNode nodes[10];
	for (int i = 0; i < 10; i++)
	{
		LINKEDLIST_InitNode(&nodes[i]);
		LINKEDLIST_add(list, &nodes[i]);
	}

	for (int i = 0; i < 10; i++)
	{
		LINKEDLIST_remove(list, &nodes[i]);
	}

	assertEqualsPtr(list->head, NULL, "Head Pointer");
	assertEqualsPtr(list->tail, NULL, "Tail Pointer");
	assertEqualsInt(list->count, 0, "List Count");
	assertEqualsPtr(nodes[0].next, NULL, "Node Next pointer");
	assertEqualsPtr(nodes[5].prev, NULL, "Node Prev Pointer");

	free(list);
}

static void TestPushandPop()
{
	info("Remove Tests");
	LinkedList * list = (LinkedList *)malloc(sizeof(LinkedList));
	LINKEDLIST_Init(list);

	LinkedListNode node;
	LINKEDLIST_InitNode(&node);

	LINKEDLIST_Push(list, &node);
	assertEqualsInt(list->count, 1, "Count after push");
	LINKEDLIST_Pop(list);
	assertEqualsInt(list->count, 0, "Count after pop");

	LINKEDLIST_Destroy(list);
}

void RunListTests(void)
{
	info("Begining List Tests");

	TestInit();
	TestNode();
	TestAdd();
	TestRemove();
	TestPushandPop();

	info("Ending List Tests");
}