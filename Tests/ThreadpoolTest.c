#include "stdafx.h"
#include "ThreadpoolTest.h"
#include "../BasicTestFramework/Log.h"

static void printlineTest(void);

static void printCount(void * arg)
{
	int inpt = (int)arg;
	fprintf(stdout, "Test %d\n", inpt);
}

static void initTest(ThreadPool * pool)
{
	info("Begin Initial Threadpool Test");
	for (int i = 0; i < 10; i++)
	{
		DelegateWorker * del = (DelegateWorker *)malloc(sizeof(DelegateWorker));
		del->args = (void *)i;
		del->func = printCount;
		POOL_exec(pool, del);
	}
	info("End initial threadpool Test");
}

void runThreadpoolTests(void)
{
	info("Starting threadpool Tests.");

	ThreadPool tp;
	POOL_Init(&tp, 8);

	initTest(&tp);

	info("Ending threadpool tests");
	return;
}