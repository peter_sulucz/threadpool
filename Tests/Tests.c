// Tests.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "../BasicTestFramework/Log.h"
#include "ListTest.h"
#include "ThreadpoolTest.h"
#include "CryptoTests.h"
#include "RouterTests.h"


int _tmain(int argc, _TCHAR* argv[])
{
	info("Starting Tests");

	RunListTests();

	runThreadpoolTests();

	RunCryptoTests();

	RunRouterTests();

	return 0;
}

