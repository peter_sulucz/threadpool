#include "stdafx.h"
#include "CryptoTests.h"
#include "../BasicTestFramework/Log.h"
#include "../Crypto/Encoding.h"
#include "../Crypto/rsa.h"
#include "../Crypto/sha256.h"
#include "../Crypto/aes.h"
#pragma warning(push, 3)
#include <string.h>
#pragma warning(pop)
#define TESTBUFFERLEN 256

static void Base64Test(void)
{
	char buffer[TESTBUFFERLEN];
	char test1[] = "5";
	char test2[] = "";
	char test3[] = "peter";
	base64Encode((char *)&test1, (char *)&buffer, (int)strlen((char *)&test1), TESTBUFFERLEN);
	assertEqualsString((char *)&buffer, "NQ==", strlen((char *)&buffer), 4, "BASE64 test: 5");

	base64Encode((char *)&test2, (char *)&buffer, strlen((char *)&test2), TESTBUFFERLEN);
	assertEqualsString((char *) &buffer, "", strlen((char *) &buffer), 0, "BASE64 test: (blank)");

	base64Encode((char *)&test3, (char *)&buffer, strlen((char *)&test3), TESTBUFFERLEN);
	assertEqualsString((char *) &buffer, "cGV0ZXI=", strlen((char *) &buffer), 8, "BASE64 test: peter");
	
	base64Decode((char *) "cGV0ZXI=", (char *) &buffer, 8, TESTBUFFERLEN);
	assertEqualsString((char *) &buffer, "peter", strlen((char *) &buffer), 5, "BASE64 Decode Test");
}

static void RSATests(void)
{
	info("Running RSA tests");
	char testString[] = "THIS IS THE TEST DATA";
	rsa_keypair * keypair = (rsa_keypair *)malloc(sizeof(rsa_keypair));
	RSA_GenerateKeypair(keypair);
	char * data = (char *)malloc(1024);
	sprintf_s(data, 1024, "%s", testString);
	int datalen = strlen(testString);
	RSA_encrypt(data, &datalen, 1024, keypair);
	RSA_decrypt(data, &datalen, keypair);

	assertEqualsString(data, testString, datalen, strlen(testString), "RSA Encrypt/Decrypt");

	DestroyKeypair(keypair);
	info("Finished RSA tests");
}

static void SHATests(void)
{
	info("Running SHA tests");
	//char buffer[TESTBUFFERLEN];
	unsigned char test [] = "PETER IS THE WORST";
	unsigned char ans[] = "5447d2a1fe8647e2e060efa316d2c474e2c5aeacddc583265c31d594bf1e3ccd";
	unsigned char sum[32];
	base16Decode(ans, ans, strlen(ans), strlen(ans) / 2);
	sha256_context ctx;
	sha256_starts(&ctx);
	sha256_update(&ctx, test, strlen(test));
	sha256_finish(&ctx, sum);
	assertEqualsString((char *) &ans, (char *) &sum, 32, 32, "SHA-256 Encrypt");
	info("Finished SHA-256 Tests");

}

static void AESTests(void)
{
	info("Running AES Tests");
	aes_context ctx;
	char key [] = "PETERSUX";
	char msg [] = "Hello";
	char out[TESTBUFFERLEN];
	aes_set_key(&ctx, key, 128);
	aes_encrypt(&ctx, msg, out);
	aes_decrypt(&ctx, out, out);
	assertEqualsString((char *) &msg, (char *) &out, strlen(msg), strlen(out), "AES Encrypt/Decrypt");
	info("Finished AES Tests");

}

void RunCryptoTests(void)
{
	info("Begginning crypto tests");

	Base64Test();

	RSATests();

	SHATests();

	AESTests();

	info("Ending crypto tests");
}