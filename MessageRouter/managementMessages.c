#include "managementMessages.h"
#include <assert.h>

typedef struct _usrSendFormat {
	unsigned long long ip;
	unsigned short port;
} UserSendFormat;

char * MANAGE_writeConnectionFrame(Router * router, User * usr, int * length)
{
	unsigned char count = (unsigned char)router->users.count;
	hashKeyVal * itr;
	usr = usr;
	int size = sizeof(unsigned char)+count * sizeof(UserSendFormat);
	if (size % 16 != 0)
		size += (16 - (size % 16));
	char * ret = (char *)calloc(size, sizeof(unsigned char));
	*ret = count;
	UserSendFormat * usf = (UserSendFormat *)(ret + 1);
	for (unsigned int i = 0; i < router->users.len; i++)
	{
		itr = router->users.hash[i];
		while (itr != NULL)
		{
			User * u = (User *)itr->value;
			usf->ip = u->endpoint.sin_addr.S_un.S_addr;
			usf->port = u->endpoint.sin_port;
			usf++;
			itr = itr->next;
		}
	}
	*length = (int)size;
	return ret;
}

char MANAGE_parseConnectionFrame(Router * router, unsigned char * frame, int length)
{
	unsigned char count = *frame;
	assert((unsigned int)length >= 1 + sizeof(UserSendFormat)* count);
	UserSendFormat * usf = (UserSendFormat *)(frame + 1);
	for (unsigned char i = 0; i < count; i++)
	{
		CONNECTION_create(router, usf->ip, usf->port);
	}
	return 1;
}