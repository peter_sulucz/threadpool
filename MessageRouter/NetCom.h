#ifndef NETCOM_H
#define NETCOM_H
#include "stdafx.h"
#include "User.h"

char LISTENER_init(Router * router, int port);

char LISTENER_destroy(Router * router);

char SEND_data(Router * router, User * user, char * data, int length);

#endif