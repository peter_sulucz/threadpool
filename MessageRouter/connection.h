#ifndef _CONNECTION_H
#define _CONNECTION_H
#include "stdafx.h"
#include "User.h"

typedef enum _messageType{
	MESG_DATA = 0x00,
	MESG_MANAGE = 0x01,
	MESG_DISCONNECT = 0x02,
	MESG_RELAY = 0x03
} MessageType;

char CONNECTION_create(Router * router, unsigned long long ip, unsigned int port);

char CONNECTION_send(Router * router, User * user, char * data, int datalen, MessageType mtype);

char CONNECTION_writeFrameTobuffer(char * buffer, int buflen, User * user, char * data, int datalen, MessageType mtype);

char CONNECTION_decryptFrame(char * buffer, int buflen, User * user);

//unsigned long long 

#endif