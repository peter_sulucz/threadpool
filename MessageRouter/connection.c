#include "connection.h"
#include "User.h"
#include "NetCom.h"
#include "../Crypto/random.h"
#include <assert.h>

/*
	IP and PORT MUST BE IN NETWORK ORDER BEFORE CALLING THIS FUNCTION
*/
char CONNECTION_create(Router * router, unsigned long long ip, unsigned int port)
{
	// create a new user with the ip
	User * nuser = (User *)malloc(sizeof(User));
	USER_init(nuser);
	nuser->endpoint.sin_port = (short)port;
	nuser->endpoint.sin_addr.S_un.S_addr = (ULONG)ip;
	nuser->endpoint.sin_family = AF_INET;

	// set conneciton state
	nuser->state = CONN_WAITINGFORPK;

	// grab the mutex and add to the hash
	WaitForSingleObject(router->usersMutex, INFINITE);

	// using ip as hash key... which does not work if their is more than one client with the same ip
	HASH_add(&router->users, ip, nuser);

	ReleaseMutex(router->usersMutex);

	// Send the data to initialize the connection.
	SEND_data(router, nuser, router->privatekey.publicKey, router->privatekey.pubkeyLength);
	return 1;
}

/*
	Write a frame to the buffer.
	buflen is the space available on the buffer.
*/
char CONNECTION_writeFrameTobuffer(char * buffer, int buflen, User * user, char * data, int datalen, MessageType mtype)
{
	// block size!!
	assert(buflen % 16 == 0);
	assert(datalen % 16 == 0);

	assert(buflen == datalen + 16); // make sure the buffer is the right size

	char header[16];
	writeRandom(header, 15); // make a header, with random bytes
	char * bufptr = buffer;


	header[15] = (char)mtype;

	// encrypt and write header
	aes_encrypt(&user->aes_ctx, (unsigned char *)&header, (unsigned char *)buffer);
	bufptr += 16;

	for (int i = 0; i < datalen; i+=16)
	{
		aes_encrypt(&user->aes_ctx, (unsigned char *)&data[i], (unsigned char *)bufptr);
		bufptr += 16;
	}
	return 1;
}

/*
	Safe All purpose send. Encrypts data and prepends information.
	Make sure data is a multiple of the block size before calling this function
*/
char CONNECTION_send(Router * router, User * user, char * data, int datalen, MessageType mtype)
{
	assert(datalen % 16 == 0); // block size!!

	char * output = (char *)malloc(16 + datalen);
	if (output == NULL) // uh oh, malloc has failed
		return 0;

	// create the frame
	CONNECTION_writeFrameTobuffer(output, 16 + datalen, user, data, datalen, mtype);

	// send the data
	SEND_data(router, user, output, 16 + datalen);
	return 1;
}

/*
	Decrypt a frame. In place.
*/
char CONNECTION_decryptFrame(char * buffer, int buflen, User * user)
{
	assert(buflen % 16 == 0);
	for (int i = 0; i < buflen; i += 16)
	{
		aes_decrypt(&user->aes_ctx, (unsigned char *)&buffer[i], (unsigned char *)&buffer[i]);
	}
	return 1;
}