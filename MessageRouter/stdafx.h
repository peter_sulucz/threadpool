#ifndef STDAFX_H
#define STDAFX_H

#include <stdlib.h>
#include <stdio.h>

#pragma warning(push, 3)
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <winsock2.h>
#pragma comment(lib, "Ws2_32.lib") // needs to exist(windows), or the linker will get pissed
#pragma warning(pop)

#include "../List/list.h"
#include "../List/hash.h"
#include "../threadpool.h"
#include "../Crypto/rsa.h"



typedef struct _listener{
	SOCKET sock;
	int port;
} Listener;

typedef struct _router
{
	rsa_keypair privatekey;
	HANDLE usersMutex;
	HashTable users;
	ThreadPool pool;
	Listener listener;
} Router;

#endif