#include "NetCom.h"
#include "../Crypto/random.h"
#include "connection.h"
#include "managementMessages.h"

static void LISTENER_listen(void * rtr);

static void handleIncoming(Router * router, unsigned long long ip, unsigned short port, char* buffer, int len);

char LISTENER_init(Router * router, int port)
{
	DelegateWorker * del = (DelegateWorker *)malloc(sizeof(DelegateWorker));
	router->listener.sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	router->listener.port = port;
	if (router->listener.sock == INVALID_SOCKET) // check valid
	{
		int err = WSAGetLastError();
		perror("Socket error");
		return (char)err;
	}
		

	del->args = router;
	del->func = LISTENER_listen;

	POOL_exec(&router->pool, del);

	return 1;
}

char LISTENER_destroy(Router * router)
{
	closesocket(router->listener.sock);
	return 1;
}

static void LISTENER_listen(void * rtr)
{
	Router * router = (Router *)rtr;
	struct sockaddr_in paddr;
	struct sockaddr_in inaddr;
	memset(&paddr, 0 ,sizeof(paddr));
	paddr.sin_family = AF_INET;
	paddr.sin_port = htons((unsigned short)router->listener.port);
	paddr.sin_addr.S_un.S_addr = htonl(INADDR_ANY);
	int berror = 0;
	if ((berror = bind(router->listener.sock, (const struct sockaddr *)&paddr, sizeof(struct sockaddr_in))) != 0)
	{
		berror = berror;
	}
	int reclen = 0;
	char * buffer = (char *)malloc(4097);
	int inlen = sizeof(struct sockaddr_in);
	while ((reclen = recvfrom(router->listener.sock, buffer, 1024, 0, (struct sockaddr *)&inaddr, &inlen)) > 0)
	{
		handleIncoming(router, inaddr.sin_addr.S_un.S_addr, inaddr.sin_port, buffer, reclen);

		buffer = (char *)malloc(4097);
	}
	if (reclen == -1)
	{
		int err = WSAGetLastError();
		err = err;
	}
	return;
}

/*
	Handle an incoming connection
*/
static void handleIncoming(Router * router, unsigned long long ip, unsigned short port, char* buffer, int len)
{
	//char aesbuff[16];
	char second; // for sym key
	char * datastart = &buffer[0];
	int datalen = len;
	// get the user
	WaitForSingleObject(router->usersMutex, INFINITE);
	User * usr = (User *)HASH_get(&router->users, ip);
	if (usr == NULL) // cant decide wether this should happen in two critical sections, or if this is fine (speed wise)
	{
		usr = (User *)malloc(sizeof(User));
		HASH_add(&router->users, ip, usr);
		USER_init(usr);
	}
	ReleaseMutex(router->usersMutex);

	switch (usr->state)
	{
		case(CONN_NONE):
			usr->endpoint.sin_port = port;
			usr->endpoint.sin_addr.S_un.S_addr = (ULONG)ip;
			usr->endpoint.sin_family = AF_INET;

			// first message is the public key
			usr->keypair.publicKey = (char *)malloc(datalen);
			usr->keypair.pubkeyLength = datalen;
			memcpy(usr->keypair.publicKey, datastart, datalen);

			//import the key
			ImportPublicKey(&usr->keypair);

			// we are now waiting on a symetric key
			usr->state = CONN_WAITINGFORSYM;

			// send the public key to the other user
			SEND_data(router, usr, router->privatekey.publicKey, router->privatekey.pubkeyLength);

			free(buffer);
			break;
		case(CONN_WAITINGFORPK) :
			// first message is the public key
			usr->keypair.publicKey = (char *)malloc(datalen);
			usr->keypair.pubkeyLength = datalen;
			memcpy(usr->keypair.publicKey, datastart, datalen);

			//import the key
			ImportPublicKey(&usr->keypair);

			char * rp = bigRandom(SYMETRICKEYSIZE/2);
			memcpy(usr->symetricKey, rp, SYMETRICKEYSIZE / 2);
			free(rp);

			SEND_data(router, usr, usr->symetricKey, SYMETRICKEYSIZE / 2);
			
			usr->state = CONN_WAITINGFORSYM;

			free(buffer);
			break;

		case(CONN_WAITINGFORSYM):
			second = 1;
			for (int i = 0; i < SYMETRICKEYSIZE / 2; i++)
			{
				if (usr->symetricKey[i] != 0)
				{
					second = 0;
					break;
				}
			}

			if (second) // second person in handshake to generate key
			{
				char *rand = bigRandom(SYMETRICKEYSIZE / 2);
				for (int i = 0; i < SYMETRICKEYSIZE / 2; i++)
					usr->symetricKey[i * 2] = buffer[i];
				for (int i = 0; i < SYMETRICKEYSIZE / 2; i++)
					usr->symetricKey[i * 2 + 1] = rand[i];
				SEND_data(router, usr, rand, SYMETRICKEYSIZE / 2);
				usr->state = CONN_CONNECTED;
			}
			else // first half of key generated, just received second half from client
			{
				for (int i = SYMETRICKEYSIZE / 2 - 1; i >= 0; i--)
					usr->symetricKey[i * 2] = usr->symetricKey[i];
				for (int i = 0; i < SYMETRICKEYSIZE / 2; i++)
					usr->symetricKey[i * 2 + 1] = buffer[i];
				usr->state = CONN_CONNECTED;	
			}

			aes_set_key(&usr->aes_ctx, (unsigned char *)usr->symetricKey, SYMETRICKEYSIZE);
			if (second)
			{
				int len;
				char * usrinfo = MANAGE_writeConnectionFrame(router, usr, &len);
				CONNECTION_send(router, usr, usrinfo, len, MESG_MANAGE);
			}
			free(buffer);
			break;
		case(CONN_CONNECTED) :
			//aes_decrypt(&usr->aes_ctx, (unsigned char *)buffer, (unsigned char *)aesbuff);
			//fprintf_s(stdout, "%16s\n", aesbuff);
			CONNECTION_decryptFrame(buffer, len, usr);
			fprintf_s(stdout, "%s\n", buffer+16);
			free(buffer);
				break;
	}
}

/*
	Raw send to user. Do not send data this way. This is unencrypted.
*/
char SEND_data(Router * router, User * user, char * data, int length)
{
	// just do a basic send
	return (char)sendto(router->listener.sock, data, length, 0, (const struct sockaddr *)&user->endpoint, sizeof(SOCKADDR_IN));
}