#ifndef _MANAGEMENT_MESSAGES_H
#define _MANAGEMENT_MESSAGES_H

#include "stdafx.h"
#include "connection.h"

char * MANAGE_writeConnectionFrame(Router * router, User * usr, int * length);

char MANAGE_parseConnectionFrame(Router * router, unsigned char * frame, int length);

#endif