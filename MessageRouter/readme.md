# Message Router #

This project is in charge of controlling actual messaging. 
Spec:
	1. Open UDP IPV6 socket.
	2. Begin liston on a port(TBD), for user.
	3. Query to router allows return of supported formats.
	4. Handshake starts with public key exchange.
		* Both must support encryption standard (DEFAULT: RSA AES)
	5. Handshake: between Alice and Bob
		* Alice(initiates), Sends Bob her public key
		* Bob response with his public key
		* Alice, generates half of a 256 bit AES key(128 bits) encrypts with with Bobs key, and sends it to Bob
		* Bob, generates the other half of the 256 bit aes key and sends it to Alice
		* Both of them assemble the key: 
			* Merge the keys: starting with first key, then switch off {firstkey[0], secondkey[0], firstkey[1], secondkey[1]...}
		* Handshake complete!!
		* Hopefully this way if one public key is lost, stuff is still safe.
	 