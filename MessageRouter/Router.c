#include "Router.h"
#include "connection.h"
#include <windef.h>

char ROUTER_init(Router * router, int port)
{
	WSADATA wsadata;
	WSAStartup(MAKEWORD(2, 2), &wsadata);
	char res = 1;
	res &= HASH_init(&router->users, 100);
	res &= POOL_Init(&router->pool, 9);
	res &= LISTENER_init(router, port);
	router->usersMutex = CreateMutex(NULL, FALSE, NULL);
	RSA_GenerateKeypair(&router->privatekey);
	return res;
}

/*
	Closes the socket.
*/
char ROUTER_destroy(Router * router)
{
	DestroyKeypair(&router->privatekey);
	WSACleanup();
	return LISTENER_destroy(router);
}

/*
	Create a connection to a peer
	Router.. the router to use
	ip.. the ip to use
	port.. port on the client
*/
char ROUTER_createConnection(Router * router, unsigned long long ip, int port)
{
	return CONNECTION_create(router, ip, port);
}

char ROUTER_broadcastALL(Router * router, char * data, int len)
{
	WaitForSingleObject(router->usersMutex, INFINITE);
	for (unsigned int i = 0; i < router->users.len; i++)
	{
		hashKeyVal * kv = router->users.hash[i];
		while (kv != NULL)
		{
			//int missinglen = (16 - len % 16);
			//realloc(data, len + missinglen);
			//for (int i = len + missinglen - 1; i >= len; i--)
			//	data[i] = 0;
			CONNECTION_send(router, kv->value, data, len, MESG_DATA);
			kv = kv->next;
		}
	}
	ReleaseMutex(router->usersMutex);
	return 1;
}