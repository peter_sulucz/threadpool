#ifndef USER_H
#define USER_H
#include "stdafx.h"
#include "../Crypto/rsa.h"
#include "../Crypto/aes.h"
#define SYMETRICKEYSIZE 256

typedef enum _CONNECTIONSTATE{
	CONN_NONE, // no connection available
	CONN_WAITINGFORPK, // waiting for user to send public key
	CONN_WAITINGFORSYM, // waiting for half of symetric key
	CONN_CONNECTED // connected to client
} CONNECTIONSTATE;

typedef struct _user{
	CONNECTIONSTATE state;
	SOCKADDR_IN endpoint;
	rsa_keypair keypair;
	LinkedListNode elem;
	char symetricKey[256];
	aes_context aes_ctx;
	
} User;

char USER_init(User * user);

#endif