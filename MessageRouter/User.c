#include "User.h"

char USER_init(User * user)
{
	user->state = CONN_NONE;
	memset(&user->endpoint, 0, sizeof(SOCKADDR_IN));
	memset(&user->symetricKey, 0, SYMETRICKEYSIZE);
	return LINKEDLIST_InitNode(&user->elem);
}