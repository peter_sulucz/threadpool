#ifndef ROUTER_H
#define ROUTER_H
#include "stdafx.h"
#include "NetCom.h"

char ROUTER_init(Router * router, int port);

char ROUTER_createConnection(Router * router, unsigned long long ip, int port);

char ROUTER_destroy(Router * router);

char ROUTER_broadcastALL(Router * router, char * data, int len);
#endif