#ifndef RSA_H
#define RSA_H

#pragma warning(push, 3)
#include <Windows.h>
#include <wincrypt.h>
#pragma comment(lib, "crypt32.lib")
#pragma warning(pop)

typedef struct _rsa_keypair{
	char * publicKey;
	char * privateKey;
	int pubkeyLength;
	int privatekeyLength;

#ifdef _WIN32
	// windows specific
	HCRYPTPROV rsaProvider;
	HCRYPTKEY rsaKey;
#endif
} rsa_keypair;

char RSA_GenerateKeypair(rsa_keypair * key);

char ImportPublicKey(rsa_keypair * key);

char RSA_encrypt(char* data, int * datalen, int buflen, rsa_keypair * keypair);

char RSA_decrypt(char * data, int * datalen, rsa_keypair * keypair);

char DestroyKeypair(rsa_keypair * key);

#endif