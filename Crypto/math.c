#include "math.h"
#include <assert.h>
#include <stdlib.h>

/*
	relies on litle endian architecture
*/
unsigned int * multiplyBIG(unsigned int * a, unsigned int alen, unsigned int * b, unsigned int blen, unsigned int * outlen)
{
	unsigned int * result = (unsigned int *)calloc(alen * blen, sizeof(unsigned int));
	*outlen = alen*blen;
	for (unsigned int i = 0; i < alen; i++)
	{
		unsigned long long av = (unsigned long long)a[i];
		for (unsigned int j = 0; j < blen; j++)
		{
			unsigned long long bv = (unsigned long long)b[j];
			unsigned long long * rv = (unsigned long long *)&result[i];
			*rv += bv*av;
		}
	}
	return result;
}

/*
	Create a big integer from a string
	base: 2 binary, 10 decimal, 16 hex, 64 base64 etc
*/
//unsigned int * createBIGInt(char * value, int length, char base)
//{
//	// base64 is the only thing implented for now..
//	assert(base == 64);
//	char * result = (char *)calloc((int)((double)length / 8.0 * 6.0), sizeof(char));
//	char * cur = result;
//	for (int i = length-4; i >= 0; i-= 3)
//	{
//		unsigned int val = 
//	}
//}