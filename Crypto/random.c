#include "random.h"
#pragma warning(push, 3)
#include <Windows.h>
#include <wincrypt.h>
#pragma comment(lib, "crypt32.lib")
#pragma warning(pop)

#ifdef _WIN32

static HCRYPTPROV hCryptProvider;

#endif

/*
	Write length octects of random to start
*/
char writeRandom(char * start, int length)
{
#ifdef _WIN32
	CryptAcquireContext(&hCryptProvider, NULL, NULL, PROV_RSA_FULL, 0);

	if (!CryptGenRandom(hCryptProvider, length, (BYTE *)start))
	{
		int lastError = GetLastError();
		lastError = lastError;
		return 0;
	}

	CryptReleaseContext(hCryptProvider, 0);
#endif
	return 1;
}

char * bigRandom(int length)
{

	char * buffer = (char *)malloc(sizeof(char)* length);

	// windows stuff...
#ifdef _WIN32
	CryptAcquireContext(&hCryptProvider, NULL, NULL, PROV_RSA_FULL, 0);
	
	if (!CryptGenRandom(hCryptProvider, length, (BYTE *)buffer))
	{
		int lastError = GetLastError();
		lastError = lastError;
		return NULL;
	}

	CryptReleaseContext(hCryptProvider, 0);
#endif

	return buffer;
}