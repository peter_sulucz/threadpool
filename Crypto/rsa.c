#include "rsa.h"

#include <assert.h>
#include <stdio.h>

#define MY_ENCODING_TYPE  (PKCS_7_ASN_ENCODING | X509_ASN_ENCODING)

#define KEYLENGTH 0x10000000 // rsa keylength hard coded to 4096 bits... LONG KEY

/*
	Generates an RSA keypair. Size is hard set to 4096 bits. Probably should be changeable
	
*/
char RSA_GenerateKeypair(rsa_keypair * key)
{
	int err; // in case of failure
	HCRYPTPROV rsaProvider;
	HCRYPTKEY rsaKey;
	DWORD pklen;
	DWORD sklen;
	if (!CryptAcquireContext(&rsaProvider, NULL, NULL, PROV_RSA_FULL, 0))
	{
		goto failure;
	}
	
	if (!CryptGenKey(rsaProvider, CALG_RSA_KEYX, KEYLENGTH | CRYPT_EXPORTABLE, &rsaKey))
		goto failure;
	if (!CryptExportKey(rsaKey, (HCRYPTKEY)NULL, PRIVATEKEYBLOB, 0, NULL, &sklen))
		goto failure;
	if (!CryptExportKey(rsaKey, (HCRYPTKEY)NULL, PUBLICKEYBLOB, 0, NULL, &pklen))
		goto failure;

	key->privateKey = (char *)malloc(sklen);
	key->publicKey = (char *)malloc(pklen);
	key->privatekeyLength = sklen;
	key->pubkeyLength = pklen;
	key->rsaProvider = rsaProvider;
	key->rsaKey = rsaKey;

	if (!CryptExportKey(rsaKey, (HCRYPTKEY)NULL, PRIVATEKEYBLOB, 0, (BYTE *)key->privateKey, &sklen))
		goto failure;
	if (!CryptExportKey(rsaKey, (HCRYPTKEY)NULL, PUBLICKEYBLOB, 0, (BYTE *)key->publicKey, &pklen))
		goto failure;

	return 1;

failure: // shit hits the fan... not sure what to do yet
	err = GetLastError();
	err = err;
	return 0;

}

char ImportPublicKey(rsa_keypair * key)
{
	HCRYPTPROV rsaProvider;
	if (!CryptAcquireContext(&rsaProvider, NULL, NULL, PROV_RSA_FULL, 0))
	{
		assert(1 == 2);
		return 0;
	}

	if (!CryptImportKey(rsaProvider, (BYTE *)key->publicKey, key->pubkeyLength, 0, 0, &key->rsaKey))
	{
		int err = GetLastError();
		err = err;
		CryptReleaseContext(	rsaProvider, 0);
		return 0;
	}

	CryptReleaseContext(rsaProvider, 0); // think you are supposed to do this. If not, definitely fix.
	return 1;
}

/*
	RSA encrypt data.
	Return 0 on failure. Probalby buflen is too small.
	datalen: length of data inside data.
	buflen: full length of data buffer
	data: char buffer containing data to encrypt. inout
	keypair: uses keypair to encrypt, must contain public key
*/
char RSA_encrypt(char* data, int * datalen, int buflen, rsa_keypair * keypair)
{
	if (!CryptEncrypt(keypair->rsaKey, (HCRYPTHASH)NULL, TRUE, CRYPT_OAEP, (BYTE *)data, (DWORD *)datalen, buflen))
	{
		// encryption failed. Length of buffer probably not sufficient
		return 0;
	}
	return 1;
}

/*
	RSA decrypt data in buffer
	data: data to decrypt
	datalen: length of data inside data buffer
	keypair: the rsa keypair to use. Must have privat key
*/
char RSA_decrypt(char * data, int * datalen, rsa_keypair * keypair)
{
	assert(keypair->privatekeyLength > 0);
	if (keypair->privatekeyLength == 0)
	{
		return 0;
	}

	if (!CryptDecrypt(keypair->rsaKey, (HCRYPTHASH)NULL, TRUE, CRYPT_OAEP, (BYTE *)data, (DWORD *)datalen))
	{
		return 0;
	}
	return 1;
}

/*
	Destroys the key and releases the keypair.
	Overwrites all data. 
	Destroys key in OS.
*/
char DestroyKeypair(rsa_keypair * key)
{
	CryptDestroyKey(key->rsaKey);
	CryptReleaseContext(key->rsaProvider, 0);
	memset(key->publicKey, 0, key->pubkeyLength);
	memset(key->privateKey, 0, key->privatekeyLength);
	free(key->publicKey);
	free(key->privateKey);
	return 1;
}