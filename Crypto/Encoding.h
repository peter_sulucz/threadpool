#ifndef ENCODING_H
#define ENCODING_H
#include <assert.h>

/*
	Base64 encoding of input, placed into output. 
	inputlen is the length of input.
	outputlen must be greater than inputlen, and ((inputlen * 8) / 6) <= outputlen
*/
char base64(char * input, char * output, int inputlen, int outputlen);
char base64Decode(char * input, char * output, int inputlen, int outputlen);

char base16Decode(unsigned char * input, unsigned char * output, int inputlen, int outputlen);
#endif