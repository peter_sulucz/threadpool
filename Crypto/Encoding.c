#include "Encoding.h"
#include "stdlib.h"
#include "stdio.h"

#define base64ToInt(C) ((C >= 48 && C <= 57) ? C + 4 : (C <= 90 && C >= 65) ? C - 65 : (C <= 122 && C >= 97) ? C-71 : C == 43 ? C + 19 : C == 47 ? 63 : 64)
#define base16charToInt(C) ((C >= 48 && C <= 57) ? C - 48 : (C >= 65 && C <= 70) ? C - 55 : (C >= 97 && C <= 102) ? C - 87 : 0)

/*
	Base64 encoding of input, placed into output.
	inputlen is the length of input.
	outputlen must be greater than inputlen, and ((inputlen * 8) / 6) <= outputlen
*/
char base64Encode(char * input, char * output, int inputlen, int outputlen)
{
	assert(input != NULL);
	char * index = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	input = input;
	output[0] = 0;
	assert(inputlen * 8 / 6 <= outputlen);
	int i;
	int encodeIndex = 0;
	for (i = 0; i < inputlen; i += 3){
		int x = input[i];
		int y = i + 1 < inputlen ? input[i + 1] : 0;
		int z = i + 2 < inputlen ? input[i + 2] : 0;

		int pattern = (x << 0x10) + (y << 0x8) + z;

		output[encodeIndex] = index[(pattern >> 3 * 6) & 0x3f];
		output[encodeIndex + 1] = index[(pattern >> 2 * 6) & 0x3f];
		output[encodeIndex + 2] = index[(pattern >> 1 * 6) & 0x3f];
		output[encodeIndex + 3] = index[(pattern >> 0 * 6) & 0x3f];
		encodeIndex += 4;
	}
	encodeIndex--;
	for (i = 0; i < (3- (inputlen % 3)); i++){
		if (encodeIndex-i >= 0)
			output[encodeIndex - i] = '=';
	}
	output[encodeIndex + 1] = '\0';
	return *output;
}

char base64Decode(char * input, char * output, int inputlen, int outputlen){
	assert(input != NULL);
	assert(inputlen * 6 / 8 <= outputlen);
	int i = 0, j = 0;
	for (i = 0; i < inputlen; i+=4){
		int a = input[i] == '=' ? 0 : (int) base64ToInt(input[i]);
		int b = input[i+1] == '=' ? 0 : (int) base64ToInt(input[i+1]);
		int c = input[i+2] == '=' ? 0 : (int) base64ToInt(input[i+2]);
		int d = input[i+3] == '=' ? 0 : (int) base64ToInt(input[i+3]);

		int pattern = (a << 3 * 6) + (b << 2 * 6) + (c << 1 * 6) + (d << 0 * 6);
		output[j] = (pattern >> 2 * 8) & 0xff;
		output[j + 1] = (pattern >> 1 * 8) & 0xff;
		output[j + 2] = (pattern >> 0 * 8) & 0xff;
		j += 3;
	}
	output[j] = '\0';
	return * output;
}

char base16Decode(unsigned char * input, unsigned char * output, int inputlen, int outputlen)
{
	unsigned char a, b;
	assert(inputlen % 2 == 0);
	assert(outputlen == inputlen / 2);
	for (int i = 0; i < inputlen; i += 2)
	{
		a = input[i];
		b = input[i + 1];
		output[i / 2] = (base16charToInt(input[i])) << 4 | (base16charToInt(input[i + 1]));
	}
		
	return 1;
}